## App iOS - MadeInMe ##
### Interfaz para el diseño de un zapato ###

Aplicación que implementa todo lo necesario para una interfaz con la que diseñar una zapato. Trabaja con un 'Modelo Dummy', los datos no son reales, no hay interacción con la API, pero sí utiliza la clase 'ModelDummy' que proporciona los datos que necesita y simula los métodos que deberá implementar el MODELO REAL que trabaje con la API, grabe en disco y haga toda la gestión de datos.

### Trabajo realizado ###

1.- PASAR EL DISEÑO DE LA 'WEB MadeInMe' AL DE LA *'APP'*: 

- La disposición de lo que yo llamo *'Paleta'* es diferente a la de la web. He querido hacerla más práctica y adaptada al toque. 

- Otro aspecto que he adaptado son las pestallas, en *'Android*' he utilizado las de *'texto y línea'* que se usan en 'pageview' y en *'iOS'* un 'SegemengControl'. Lo he complementado con un 'MenuPath' para añadir los 'Adornos y Extras'. 

- También he añadido funcionalidades al zapato de *toque* para seleccionar distintas partes, y de *'pannig'* para mover el ángulo del zapato.

2.- PROGRAMACIÓN: Toda el interfaz. La totalidad del código es propio, realizado por mí a excepción del *'Gridwiew con secciones'* y le *'Menu Path'* en el caso de Android, y el 'Menu Path' en  'iOS'. Es código fuente 'Open Source' y especifico el origen.

3.- MODELO DUMMY: la interfaz para alimentarse de los datos tira de métodos que implementa la clase *'ModelDummy*'. Son públicos y deberán ser los que implemente el  'MODELO REAL' definitivo. De esta manera el resto del código no se vera afectado con la incorporación del 'MODELO REAL' que trabaje con la API, el disco y lo que crea oportuno para la completa gestión de los datos. 


### Instrucciones app ###

  Interfaz para el diseño de un zapato. Se utiliza un 
  **'Modelo Dummy'** que porporciona *datos ficticios* sin conexión con la API.
  La clase 'ModelDummy' provee de datos necesarios a la app y tiene los 
  métodos públicos necesarios que son los que deberán tener el 'MODELO REAL' 
  que trabaje con la API y gestione los datos. 
  
  Pasos para diseñar zapato:

   	1.- Ir seleccionando las 'Partes del zapato' (con las pestañas ó sobre zapato).
   	2.- Elegir un 'Tipo' de los que ofrece cada parte.
   	3.- A su vez cada tipo despliega una o más 'Subpartes', que son las que hay que diseñar.
   	4.- Finalmente se aplica el 'Material' de nuestra preferencia.

Se puede **girar el zapato** y elegir una **parte** tocando sobre él.

El **tipo** que se elige se muestra tambien en la parte de arriba. 

Los materiales seleccionados por el usuario de van guardando en **'Mis Materiales'**.

El **'Estado de la configuración'** o progreso indica el avance en porcentaje del diseño , barra de progreso y  circulo de progreso.



 
 
# Modelo #

Los datos (imágenes) son fijas, estan en el directorio 'assets' y el de 'drawer' en recursos. 

En la mayoría de los casos las he sacado **directamente de la web de 'MadeInMe'** (Tipos, Subpartes, Materiales; distintos ángulos del zapato, logo). 

En otros las he **manipulado** *(logo de '*Ver detalles*)* o **creado** *(background, adornos y extras de pathMenu, trazo del selector sobre el zapato)*  con Gimp, illustrator en windows y sheashore en Mac (NO PUDE UTILIZAR LOS EDITORES QUE USAMOS EN CLASE DE DISEÑO PORQUE EL PERIODO DE PRUEBA HABÍA PASADO ?!!)

 
### Uso del modelo ###
 
 Para cada 'collection view' inicializa el módelo utilizando como parámetros indices que representan las posiciones actuales:*familia, parte, tipo y subparte.*
 
       -> modelTypes 
	= ...initWithTypesOfShoeFamily:mShoeFamilyCurrent 
	shoepartIndex:shoepartIndex];
       -> modelSubparts 
	= ...initWithShoeSubpartsOfOfShoeFamily:mShoeFamilyCurrent 
	shoepartIndex:shoepartIndex typeIndex:typeOfShoepartIndex];
       -> self.modelMaterials 
	= ...initWithMaterialsOfShoeFamily:mShoeFamilyCurrent 
	shoepartIndex:shoepartIndex typeIndex:typeIndex 
	subpartIndex:subpartIndex];
       -> self.modelMyMaterials 
	= ...initWithMyMaterialsOfShoeFamily:mShoeFamilyCurrent];
 
 
 
## Métodos a implementar en el MODELO FINAL ##
 
	  // init
	  -(id) initWithTypesOfShoeFamily:(int)familyIndex shoepartIndex:
	(int)shoepartIndex;
	  -(id) initWithShoeSubpartsOfOfShoeFamily:(int)familyIndex 
	shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
	  -(id) initWithMaterialsOfShoeFamily:(int)familyIndex 
	shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex 
	subpartIndex:(int)subpartIndex;
	  -(id) initWithMyMaterialsOfShoeFamily:(int)familyIndex;
	 
	  // Load datos
	  -(NSDictionary ) loadTypesOfShoeFamily:(int)familyIndex 
	shoepartIndex:(int)shoepartIndex;
	  -(NSDictionary ) loadSubpartsOfShoeFamily:(int)familyIndex 
	shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
	  -(NSDictionary ) loadMaterialsOfShoeFamily:(int)familyIndex 
	shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex 
	subpartIndex:(int)subpartIndex ;  -(NSDictionary ) loadMyMaterialsOfShoeFamily:(int)familyIndex;
	 
	 
	   // Otros
	  -(NSInteger) countItems:(NSInteger) section;
	  -(NSInteger) countSections;
	 
	  -(void) addItemFromModel:(ModelDummy )model indexPath:
	(NSIndexPath )indexPath;
	  -(UIImage ) getItemImage:(NSIndexPath )indexPath;
	  -(UIImage ) loadItemImageView:(NSDictionary ) dataDictionary 
	indexPath:(NSIndexPath )indexPath;
	 
	  -(UIImage ) getShoeRenderNextSequenceAngle:(int)sequence;
	  -(NSString ) getSectionName:(int) section;
	  -(NSString ) getTAG;
	 
 
 
 
### Métodos Principales ###
 
   Para mostrar los diferentes collectionView se utilizan los métodos:
 
       // Vista para 'tipos' de una parte del zapato.
	(JMFCollectionView, titulo y llama a 'subpartes')
       -(void)showTypesOfShoepartIndex:(int)shoepartIndex
 
       // Vista para 'subpartes' de un 'tipo'.(JMFCollectionView y 
	llama a 'materiales')
       -(void)showShoeSubpartsWithShoepartIndexPath:
	(int)shoepartIndex shoeTypeIndex:(int)typeOfShoepartIndex  {
 
       // Vista para 'materiales' de una 'subparte'.
	(JMFCollectionView y llama a 'materiales')
       -(void)showShoeMaterialsWithShoepartIndex:(int)shoepartIndex 
	typeIndex:(int)typeIndex  subpartIndex:(int)subpartIndex {
 
       // Vista para 'materiales' que se van seleccionando
       -(void)showShoeMyMaterialsWithShoepartIndexPath:
	(int)shoepartIndex {
 
 
 
 
## JMFDesignViewController.m ##

### View's ###
 
   Se crean cuatro collectionView por medio de la clase 
JMFCollectionView: *('Types', 'Subparts' y Materials' además de 'My Materials')*

       // Muestra los diferentes TIPOS de una PARTE del zapato (diferentes punteras por ejemplo)
       @property(nonatomic, strong) JMFCollectionView 
	shoeTypesOfShoePartCollectionView;
 
       // Subpartes para un tipo
       @property(nonatomic, strong) JMFCollectionView 
	shoeSubpartsCollectionView;
 
       // Materiales para una subparte
       @property(nonatomic, strong) JMFCollectionView 
	shoeMaterialsCollectionView;
 
       // Materiales que se van seleccionando.
       @property(nonatomic, strong) JMFCollectionView 
	shoeMyMaterialsCollectionView;
 
La elección de la parte del zapato además de tocando el zapato se puede hacer por medio de un *'Segment'* 

	  // SegmentControl: (Partes del zapato a selecionar:Cuerpo,Puntera,Trasera,...)
	  @property (weak, nonatomic) IBOutlet UISegmentedControl shoePartsSegmentControl;
 
 
### Inicialización ###
 
Se guarda posiciones(Item) actuales de cada 'collections view'.
 
       int mShoeFamilyCurrent;
       int mShoeAngleCurrent;
       int mShoepartIndexCurrent;
 
       @property(nonatomic, strong) NSMutableDictionary 
	shoeTypeIndexPathCurrentDictionary;
       @property(nonatomic, strong) NSMutableDictionary shoeSubpartIndexPathCurrentDictionary;
       @property(nonatomic, strong) NSMutableDictionary 
	shoeMaterialsIndexPathCurrentDictionary;


### Protocolos ###
 
	   // CollectionsView: Tipos, Subpartes, Materiales y Mis materiales    
	-(void)onClickItemChangedCollectionViewWithTag:(NSString )tag 
	itemIndexPath:(NSIndexPath )itemIndexPath
	 
	   // Devuelve el nombre de la sección para la collection view
	   -(NSString )onGetSectionNameWithTag:(int)section tag:(NSString 
	)tag
	 
	   // SegmentControl: Partes
	    - (IBAction)onClickItemChangedShoepartsSegmentControl:
	(id)sender
	 
	   // MenuPath: Adornos y Extras
	    - (IBAction) onClickItemChangedMenuPath:(id)sender
	 
	   // Cambia la orientación del zapato
	   - (IBAction)onShoePanGestures:(id)sender
	 
	   // Seleciona las distintas partes tocando el zapato
	   - (IBAction)onShoeTapGestures:(id)sender
	 
	   // Despliega los detalles configurados
	   - (IBAction)onClickDetailsButton:(id)sender
	 
	   // Boton de compra
	   - (IBAction)onClickBuyButton:(id)sender
	 
	 
 
 
## JMFCollectionView.m ##
 
  Controla una collection view proporcionandole:

- la collectionview (vista a rellenar), 
- un modelo,
- directionScroll,
- delegado (->ItemChanged), 
- una etiqueta (título) 
- y elemento actual (item check).
 
		-----------
		Protocolo
		-----------
            -(void)collectionViewChangedWithTag:(NSString )tag itemIndexPath:(NSIndexPath )itemIndexPath;
 
		-----------
		Parámetros
		-----------
		initWithCollectionView:
		(UICollectionView)collectionView --> Vista del collectionview.
		
		model:(id)model --> Modelo (métodos: countItems, countSections, image)
		
		setScrollDirection:(UICollectionViewScrollDirection) scrollDirection  --> ScrollDirection
		
		delegate:(id)aDelegate  --> Delegado                                  
		
		tag:(NSString )tag    --> Etiqueta (title).
		
		itemIndexPathCurrent:(NSIndexPath )itemIndexPathCurrent --> Elemento actual        
	
 
 



//
//  JMFDesignViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//
/* ....................................................................................
 *
 *  ====================
 *         VISTA
 *  ====================
 *
 *      Interface necesaria para diseñar un zapato. Trabaja con
 *  datos 'Dummy'(no reales, sín interación con la API). Para ello se 
 *  utiliza un modelo (ModelDummy) que hace uso de los métodos que deberá 
 *  implementar el MODELO FINAL que sí interactue con la API, asi como el
 *  con disco y base de datos.
 *
 *      Mediante views (segment, collectionView,...) se accede a las
 *  diferentes opciones para configurar el zapato.
 *
 *     Para seleccionar las distintas partes del zapato se puede hacer
 *  desde el 'SegmentControl' o tocando directamente sobre la imagen del
 *  zapato.
 *
 *     La posición del zapato se puede cambiar haciendo 'Panning' sobre el 
 *  zapato.
 *
 *
 *  ______________________________________________________________
 *  |
 *  | -----------------------
 *  || SEGMENT CTRL (Partes) |<- Alternativa I
 *  | -----------------------
 *  | --  -------------
 *  ||  ||             -> CollectionView2 (Opciones de un tipo)
 *  ||  | ------------
 *  ||  | # # #
 *  ||<- CollectionnView1 (Tipos de una parte)
 *  ||  |
 *  ||  | # # # # # #
 *  ||  | # # # # # # -> CollectionView3
 *  ||  | # # # # # #      (Materiales)
 *  | --          ----------------------------
 *  |            | Segment ctl (Partes)     |  <- Alternativa II
 *  ________________________________________________________________
 
 *
 *  ========
 *   View's
 *  ========
 *
 *
 * // SegmentControl: (Partes del zapato a selecionar:Cuerpo,Puntera,Trasera,...)
 * @property (weak, nonatomic) IBOutlet UISegmentedControl *shoePartsSegmentControl;
 *
 *  Se crean cuatro collectionView por medio de la clase JMFCollectionView:
 *  'Types', 'Subparts' y Materials' además de 'My Materials'
 *
 *      // Muestra los diferentes TIPOS de una PARTE del zapato (diferentes punteras por ejemplo)
 *      @property(nonatomic, strong) JMFCollectionView *shoeTypesOfShoePartCollectionView;
 *
 *      // Subpartes para un tipo
 *      @property(nonatomic, strong) JMFCollectionView *shoeSubpartsCollectionView;
 *
 *      // Materiales para una subparte
 *      @property(nonatomic, strong) JMFCollectionView *shoeMaterialsCollectionView;
 *
 *      // Materiales que se van seleccionando.
 *      @property(nonatomic, strong) JMFCollectionView *shoeMyMaterialsCollectionView;
 *
 *
 *
 *   ================
 *    Inicialización
 *   ================
 *
 *      
 *    Hay cuatro 'collection view' donde se muestran:
 *   Tipos, Subpartes y Materiales además de 'Mis materiales".
 *
 *   Se guarda posiciones(Item) actuales de cada 'collections view'.
 *
 *      int mShoeFamilyCurrent;
 *      int mShoeAngleCurrent;
 *      int mShoepartIndexCurrent;
 *
 *      @property(nonatomic, strong) NSMutableDictionary *shoeTypeIndexPathCurrentDictionary;
 *      @property(nonatomic, strong) NSMutableDictionary *shoeSubpartIndexPathCurrentDictionary;
 *      @property(nonatomic, strong) NSMutableDictionary *shoeMaterialsIndexPathCurrentDictionary;
 *
 *
 *
 *  ======
 *  MODELO
 *  ======
 *
 *  Se utiliza un modelo de pruebas:
 *
 *    Para cada 'collection view' inicializa el módelo utilizando como
 *   parámetros indices que representan las posiciones actuales: familia, parte, tipo y subparte.
 *
 *      -> modelTypes = ...initWithTypesOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex];
 *      -> modelSubparts = ...initWithShoeSubpartsOfOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeIndex:typeOfShoepartIndex];
 *      -> self.modelMaterials = ...initWithMaterialsOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeIndex:typeIndex subpartIndex:subpartIndex];
 *      -> self.modelMyMaterials = ...initWithMyMaterialsOfShoeFamily:mShoeFamilyCurrent];
 *
 *
 *
 *  ==============================================
 *  ** Métodos a implementar en el MODELO FINAL **
 *  ==============================================
 *
 * // init
 * -(id) initWithTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex;
 * -(id) initWithShoeSubpartsOfOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
 * -(id) initWithMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex;
 * -(id) initWithMyMaterialsOfShoeFamily:(int)familyIndex;
 *
 * // Load datos
 * -(NSDictionary *) loadTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex;
 * -(NSDictionary *) loadSubpartsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
 * -(NSDictionary *) loadMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex ;
 * -(NSDictionary *) loadMyMaterialsOfShoeFamily:(int)familyIndex;
 *
 *
 *  // Otros
 * -(NSInteger) countItems:(NSInteger) section;
 * -(NSInteger) countSections;
 *
 * -(void) addItemFromModel:(ModelDummy *)model indexPath:(NSIndexPath *)indexPath;
 * -(UIImage *) getItemImage:(NSIndexPath *)indexPath;
 * -(UIImage *) loadItemImageView:(NSDictionary *) dataDictionary indexPath:(NSIndexPath *)indexPath;
 *
 * -(UIImage *) getShoeRenderNextSequenceAngle:(int)sequence;
 * -(NSString *) getSectionName:(int) section;
 * -(NSString *) getTAG;
 *
 *
 *
 *
 *  ======================
 *    Métodos Principales
 *  ======================
 *
 *  Para mostrar los diferentes collectionView se utilizan los métodos:
 *
 *      // Vista para 'tipos' de una parte del zapato.(JMFCollectionView, titulo y llama a 'subpartes')
 *      -(void)showTypesOfShoepartIndex:(int)shoepartIndex
 *
 *      // Vista para 'subpartes' de un 'tipo'.(JMFCollectionView y llama a 'materiales')
 *      -(void)showShoeSubpartsWithShoepartIndexPath:(int)shoepartIndex shoeTypeIndex:(int)typeOfShoepartIndex  {
 *
 *      // Vista para 'materiales' de una 'subparte'.(JMFCollectionView y llama a 'materiales')
 *      -(void)showShoeMaterialsWithShoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex  subpartIndex:(int)subpartIndex {
 *
 *      // Vista para 'materiales' que se van seleccionando
 *      -(void)showShoeMyMaterialsWithShoepartIndexPath:(int)shoepartIndex {
 *
 *
 *
 *
 *  =============
 *    Protocolos
 *  =============
 *
 *  // CollectionsView: Tipos, Subpartes, Materiales y Mis materiales
 *   -(void)onClickItemChangedCollectionViewWithTag:(NSString *)tag itemIndexPath:(NSIndexPath *)itemIndexPath
 *
 *  // Devuelve el nombre de la sección para la collection view
 *  -(NSString *)onGetSectionNameWithTag:(int)section tag:(NSString *)tag
 *
 *  // SegmentControl: Partes
 *   - (IBAction)onClickItemChangedShoepartsSegmentControl:(id)sender
 *
 *  // MenuPath: Adornos y Extras
 *   - (IBAction) onClickItemChangedMenuPath:(id)sender
 *
 *  // Cambia la orientación del zapato
 *  - (IBAction)onShoePanGestures:(id)sender
 *
 *  // Seleciona las distintas partes tocando el zapato
 *  - (IBAction)onShoeTapGestures:(id)sender
 *
 *  // Despliega los detalles configurados
 *  - (IBAction)onClickDetailsButton:(id)sender
 *
 *  // Boton de compra
 *  - (IBAction)onClickBuyButton:(id)sender
 *
 *
 *
 *
 *  ====================
 *    JMFCollectionView
 *  ====================
 *
 *      Controla una collection view proporcionaldole la collectionview (vista a rellenar), un modelo, directionScroll, delegado (->ItemChanged), una etiqueta (título) y elemento actual (item check).
 *
 *      -----------
 *      Protocolo
 *      -----------
 *          -> -(void)collectionViewChangedWithTag:(NSString *)tag itemIndexPath:(NSIndexPath *)itemIndexPath;
 *
 *      -----------
 *      Parámetros
 *      -----------
 *          initWithCollectionView:(UICollectionView *) collectionView:     --> Vista del collectionview.
 *          model:(id)model:                                                --> Modelo (métodos: countItems, countSections, image).
 *          setScrollDirection:(UICollectionViewScrollDirection) scrollDirection  --> ScrollDirection.
 *          delegate:(id)aDelegate                                          --> Delegado.
 *          tag:(NSString *)tag                                             --> Etiqueta (title).
 *          itemIndexPathCurrent:(NSIndexPath *)itemIndexPathCurrent        --> Elemento actual.
 *
 *
 .........................................................................................*/
#import <QuartzCore/QuartzCore.h>
#import <math.h>

#import "JMFDesignViewController.h"
#import "ModelDummy.h"
#import "JMFCollectionViewCell.h"
#import "JMFCollectionView.h"
#import "JMFSignupDummyViewController.h"

#import "MenuPath.h"
#import "TYMProgressBarView.h"
#import "Utils.h"
#import "Colors.h"

const static int FAMILIA_BAILARINA         = 0;
const static int FAMILIA_SALON             = 1;
const static int FAMILIA_MTACON            = 2;
const static int FAMILIA_MPLANO            = 3;
const static int FAMILIA_SANDALIATACON = 4;
const static int FAMILIA_SANDALIAPLANA = 5;
const static int FAMILIA_BOTINCORDONES = 6;


// Pestañas zapato.
static int const SHOEPART_BODY             = 0;
static int const SHOEPART_TOECAP            = 1;
static int const SHOEPART_BACK             = 2;
static int const SHOEPART_HEEL             = 3;
static int const SHOEPART_ORNAMENT         = 4;
static int const SHOEPART_ORNAMENT_TAPES   = 5;    // Ornamentos cintas.
static int const SHOEPART_ORNAMENT_FRONT   = 6;    // Ornamentos delanteros.
static int const SHOEPART_ORNAMENT_BACK    = 7;    // Ornamentos traseros.
static int const SHOEPART_EXTRAS           = 8;


static int const DIRECTION_PANNING_DCHA     = 1;
static int const DIRECTION_PANNING_IZQ      = -1;

static NSString * const SHOE_PARTS_ARRAY_TITLE[] = {
    @"Cuerpo",
    @"Puntera",
    @"Trasera",
    @"Tacon",
    @"Adorno",
    @"Adornos - Cintas",
    @"Adornos - Delanteros",
    @"Adornos - Traseros",
    @"Extras"};


static NSString *const TAG_TYPE_OF_SHOE_PART = @"viewTypeOfShoePart";
static NSString *const TAG_SHOE_SUBPART = @"viewShoeSubpart";
static NSString *const TAG_SHOE_MATERIALS = @"viewMaterials";
static NSString *const TAG_SHOE_MYMATERIALS = @"viewMyMaterials";


@interface JMFDesignViewController () {
    
    int mTouchXviewPositionInitial;
    int mTouchYviewPositionInitial;
    
    // Posiciones actuales
    int mShoeFamilyCurrent;
    int mShoeAngleCurrent;
    int mShoepartIndexCurrent;
    int mMyMaterialsIndexCurrent;
    
    UIView *mSelectorViewCurrent;
    
    Boolean mIsShowDetails;
}

// MODELO
@property(nonatomic,strong) ModelDummy *modelMaterials;
@property(nonatomic,strong) ModelDummy *modelMyMaterials;


// Guarda posiciones(Item) actuales.
@property(nonatomic, strong) NSMutableDictionary *shoeTypeIndexPathCurrentDictionary;
@property(nonatomic, strong) NSMutableDictionary *shoeSubpartIndexPathCurrentDictionary;
@property(nonatomic, strong) NSMutableDictionary *shoeMaterialsIndexPathCurrentDictionary;

// Collecionviews para: 'Types', 'Subparts' y Materials'
@property(nonatomic, strong) JMFCollectionView *shoeTypesOfShoePartCollectionView;
@property(nonatomic, strong) JMFCollectionView *shoeSubpartsCollectionView;
@property(nonatomic, strong) JMFCollectionView *shoeMaterialsCollectionView;
@property(nonatomic, strong) JMFCollectionView *shoeMyMaterialsCollectionView;


@end

@implementation JMFDesignViewController

// Menu Path
@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize button4;
@synthesize button5;
@synthesize main;
@synthesize navigation;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        mShoeFamilyCurrent = FAMILIA_SALON;
        mIsShowDetails = NO;
        mMyMaterialsIndexCurrent = -1;

        
        /* ------------------
         *
         *  Modelo
         *
         *  Init MyMaterials
         *
           ------------------*/
        self.modelMyMaterials = [[ModelDummy alloc] initWithMyMaterialsOfShoeFamily:mShoeFamilyCurrent];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    /* -----------------
     * Posiciones index
       ----------------- */
    self.shoeTypeIndexPathCurrentDictionary =  [NSMutableDictionary new];
    self.shoeSubpartIndexPathCurrentDictionary =  [NSMutableDictionary new];
    self.shoeMaterialsIndexPathCurrentDictionary =  [NSMutableDictionary new];
    [self showTypesOfShoepartIndex:SHOEPART_BODY];
    
    
    /* --------------------------------------
     * Menú path animation: Adornos y Extras
      --------------------------------------- */
    // initialize ExpandableNavigation object with an array of buttons.
    NSArray* buttons = [NSArray arrayWithObjects:button1, button2, button3, button4, button5, nil];
    self.navigation = [[MenuPath alloc] initWithMenuItems:buttons mainButton:self.main radius:130.0];
//    self.navigation = [[ExpandableNavigation alloc] initWithMenuItems:buttons mainButton:self.main radius:120.0];
    
    
    /* --------------------------
     * Estado de la configuración
       -------------------------- */
    CGFloat offsetX = 0.0f;
    CGFloat offsetY = 0.0f;
    CGFloat width = (self.progressView.bounds.size.width - offsetX * 2);
    
    TYMProgressBarView *progressBarView = [[TYMProgressBarView alloc] initWithFrame:CGRectMake(offsetX, offsetY, width, 16.0f)];
    progressBarView.barBorderWidth = 2.0f;
    progressBarView.barBorderColor = [Utils colorWithHexString:COLOR_MadeInMeDarck];
    progressBarView.barFillColor = [Utils colorWithHexString:COLOR_MadeInMeDarck];
    
    // Add it to your view
    [self.progressView addSubview:progressBarView];
    
    // Set the progress
    progressBarView.progress = 0.5f;
    
    
    
    /* -----------------------
     * Colors & Skin of view's
      ------------------------ */
    self.lblTypes.tintColor = [Utils colorWithHexString:COLOR_MadeInMe];
    self.shoePartsSegmentControl.tintColor = [Utils colorWithHexString:COLOR_MadeInMeDarck];
    
    self.linePaletteTypes.backgroundColor = [Utils colorWithHexString:COLOR_MadeInMeDarck];
    //    self.collectionViewTypesOfShoePart.layer.borderColor = [Utils colorWithHexString:COLOR_MadeInMe].CGColor;
    //    self.collectionViewTypesOfShoePart.layer.borderWidth = 1;
    
    //    self.collectionViewShoeMaterials.layer.backgroundColor = [Utils colorWithHexString:COLOR_MadeInMegrisLight].CGColor;
    
    // Progress
    self.progressLabel.backgroundColor = [Utils colorWithHexString:COLOR_MadeInMeDarck];
    [self.progressLabel.layer setCornerRadius:7];
    
    
    /* --------
     * Detalles
       -------- */
    self.detailsDummyLabel.transform = CGAffineTransformMakeRotation(3.14/-10.5);

    
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    
    
    /* --------------------------------
     * Adorrnos y Extras: Menu path
     ---------------------------------- */
    // Release any retained subviews of the main view.
    self.button1 = nil;
    self.button2 = nil;
    self.button3 = nil;
    self.button4 = nil;
    self.button5 = nil;
    self.main = nil;
    self.navigation = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Protocolos propios

/** ................................................
 *
 *  Listener de collection view: (JMFCollectionView)
 *      Types, Subparts, Materials & MyMaterials.
 *
 ...................................................*/
-(void)onClickItemChangedCollectionViewWithTag:(NSString *)tag itemIndexPath:(NSIndexPath *)itemIndexPath {
    
    /* ----------------------------------
     * collectionView para las types
     ------------------------------------*/
    if (tag == TAG_TYPE_OF_SHOE_PART) {
        
        // Guarda estado tipo, subparte y materiales).
        self.shoeTypeIndexPathCurrentDictionary[@(mShoepartIndexCurrent)] = itemIndexPath;
        self.shoeSubpartIndexPathCurrentDictionary[@(mShoepartIndexCurrent)] = [NSIndexPath indexPathForRow:0 inSection:0];
       
        
        /*
         * Subparts
         */
        [self showShoeSubpartsWithShoepartIndexPath:mShoepartIndexCurrent shoeTypeIndex:itemIndexPath.row];

        
        /* ----------------------------------
         * collectionView para las subpartes
         ------------------------------------*/
    } else if (tag == TAG_SHOE_SUBPART) {
        
        // Guarda estado subpartes y materiales.
        self.shoeSubpartIndexPathCurrentDictionary[@(mShoepartIndexCurrent)] = itemIndexPath;
       
        
        NSIndexPath *typeIndexPath = [self.shoeTypeIndexPathCurrentDictionary objectForKey:@(mShoepartIndexCurrent)];
        [self showShoeMaterialsWithShoepartIndex:mShoepartIndexCurrent typeIndex:typeIndexPath.row subpartIndex:itemIndexPath.row];
        
        /* ----------------------------------
         * collectionView para los materiales
         ------------------------------------*/
    } else if (tag == TAG_SHOE_MATERIALS) {
        
        
        [self setItemMaterialsWithIndexPath:itemIndexPath];
        [self.modelMyMaterials addItemFromModel:self.modelMaterials indexPath:itemIndexPath];
        
        
        /*
         * Mis Materials
         */
        mMyMaterialsIndexCurrent = -1;  // Deselecciona mis materiales
        [self showShoeMyMaterialsWithShoepartIndexPath:mShoepartIndexCurrent];
        
        
        
        
    } else if (tag == TAG_SHOE_MYMATERIALS) {
        
        mMyMaterialsIndexCurrent = itemIndexPath.row;
    }

}





/** ........................................................
 *
 * Devuelve el nombre de la sección para la collection view
 *
 ...........................................................*/
-(NSString *)onGetSectionNameWithTag:(int)section tag:(NSString *)tag {
    
    NSString *sectionName;
    if (tag == TAG_SHOE_MATERIALS)
        sectionName = [self.modelMaterials getSectionName:section];
    
    
    return sectionName;
}




/** .........................................
 *
 *  Listener de SegementControl: Shoeparts
 *
 ............................................*/

- (IBAction)onClickItemChangedShoepartsSegmentControl:(id)sender {
    
    mShoepartIndexCurrent = self.shoePartsSegmentControl.selectedSegmentIndex;
    [self showTypesOfShoepartIndex:mShoepartIndexCurrent];
    
}


/* ......................................
 *
 * Listener Menu Path: Adornos y Extras
 *
 ........................................*/
- (IBAction) onClickItemChangedMenuPath:(id)sender {
    
    int index = [[(UIButton *)sender currentTitle] intValue];
    int shoepartIndex = -1;
    [self.shoePartsSegmentControl setSelectedSegmentIndex:-1];
    
    /*
     * Adornos
     */
    if (index == 0)
        shoepartIndex = SHOEPART_ORNAMENT_FRONT;
    else if (index == 1)
        shoepartIndex = SHOEPART_ORNAMENT_BACK;
    else if(index == 2 || index == 3)
        shoepartIndex = SHOEPART_ORNAMENT_TAPES;
    
    /*
     * Extras
     */
    else if(index == 4)
        shoepartIndex = SHOEPART_EXTRAS;
    
    if (shoepartIndex != -1) {
        mShoepartIndexCurrent = shoepartIndex;
        [self showTypesOfShoepartIndex:shoepartIndex];
    }
    
    
    if( self.navigation.expanded ) {
        [self.navigation collapse];
    }
}



/* ......................................
 *
 * Cambia la orientación del zapato
 *
 ........................................*/
- (IBAction)onShoePanGestures:(id)sender {
    
//    UIGestureRecognizer *gesture = (UIGestureRecognizer*)sender;
    
    // Coordenadas en la vista del zapato
    int xView = [sender locationInView:self.shoeImage].x;
    int yView = [sender locationInView:self.shoeImage].y;
    
    UIPanGestureRecognizer *gesturePan = (UIPanGestureRecognizer*)sender;
    if (gesturePan.state != UIGestureRecognizerStateBegan) {
        
        int withView = self.shoeImage.bounds.size.width;
        int heightView = self.shoeImage.bounds.size.height;
        
        int sensorPanning = 100;    //Utils.isTablet(this) ? 100 : 85;
        Boolean dchaViewPosition = xView > withView/2;
        Boolean topViewPosition = yView < heightView/3;
            
            
        
        /** ------------ EJE X ------------------
         *
         *  Si se a desplazado el dedo (Panning)
         *
         --------------------------------------*/
            if (abs(mTouchXviewPositionInitial - xView) > sensorPanning) {
                
				/* -------------------------
				 *  Panning de dcha a izq.
				 ---------------------------*/
				if (mTouchXviewPositionInitial > xView) {
					if (!topViewPosition)
                        [self moveShoe:DIRECTION_PANNING_IZQ];
					else
                        [self moveShoe:DIRECTION_PANNING_DCHA];
				}
				/* -----------------------
				 *  Panning de izq a dcha.
                 ------------------------*/
				else {
					if (!topViewPosition)
                    [self moveShoe:DIRECTION_PANNING_DCHA];
					else
                    [self moveShoe:DIRECTION_PANNING_IZQ];
				}
                
				// Actualizacion
				mTouchXviewPositionInitial = xView;

            }
            
            
			/** ------------ EJE Y -----------------
			 *
			 *  Si se a desplazado el dedo (Panning)
			 *
             -------------------------------------*/
			if (abs(mTouchYviewPositionInitial - yView) > sensorPanning
                ||
                (abs(mTouchYviewPositionInitial - xView) > sensorPanning/2
                 && abs(mTouchYviewPositionInitial - yView) > sensorPanning/2)) {
                    
//                    panning = true;
//                    if (mSelectorViewCurrent != null)
//					mSelectorViewCurrent.setVisibility(View.INVISIBLE);
                    
                    /* ------------------
                     *  De abajo a arriba.
                     ------------------*/
                    if (mTouchYviewPositionInitial > yView) {
                        if (!dchaViewPosition)
                            [self moveShoe:DIRECTION_PANNING_IZQ];
                        else
                            [self moveShoe:DIRECTION_PANNING_DCHA];
                    }
                    
                    /* ------------------
                     *  De arriba a abajo.
                     ------------------*/
                    else {
                        if (!dchaViewPosition)
                            [self moveShoe:DIRECTION_PANNING_DCHA];
                        else
                            [self moveShoe:DIRECTION_PANNING_IZQ];
                    }
                    
                    // Actualizacion
                    mTouchYviewPositionInitial = yView;
                }

    }
    
}



/* ..................................................
 *
 * Seleciona las distintas partes tocando el zapato
 *
 ....................................................*/
- (IBAction)onShoeTapGestures:(id)sender {

//    int xScreen = [sender locationOfTouch:<#(NSUInteger)#> inView:<#(UIView *)#>:self.shoeImage].x;
//    int yScreen = [sender locationInView:self.shoeImage].y;
    
    
    CGPoint tapPoint = [sender locationInView:self.view];
    
    // Coordenadas en la vista del zapato
    int xView = [sender locationInView:self.shoeImage].x;
    int yView = [sender locationInView:self.shoeImage].y;
    int withView = self.shoeImage.bounds.size.width;
    int heightView = self.shoeImage.bounds.size.height;
    
    
    int sectionsXYViewPrecision = 8;  	// 8x8
//    int sectionsXYView = 3;			 	// Se utiliza para identificar areas m·s grandes.


    // Columna y fila con 'n' secciones = sectionsXYViewPrecision.
    int colViewPrecision = xView / (withView/sectionsXYViewPrecision);
    int rowViewPrecision = yView / (heightView/sectionsXYViewPrecision);

    
//    NSLog(@"------------------");
//    NSLog(@"ANGLE: %d", mShoeAngleCurrent);
//    NSLog(@"------------------");
//    NSLog(@"colViewPrecision: %d", colViewPrecision);
//    NSLog(@"rowViewPrecision: %d", rowViewPrecision);
//    NSLog(@"colView1: %d", colView1);
//    NSLog(@"rowView1: %d", rowView1);
//    NSLog(@"colView2: %d", colView2);
//    NSLog(@"rowView2: %d", rowView2);
//    NSLog(@"tapPointX: %f", tapPoint.x);
//    NSLog(@"tapPointY: %f", tapPoint.y);
//    NSLog(@"tapPointInViewX: %f", tapPointInView.x);
//    NSLog(@"tapPointInView: %f", tapPointInView.y);
    
    /* ---------------
     * Sección pulsada
       ---------------*/
    int shoepartIndex = [self getShoepartTouch:mShoeAngleCurrent colView:colViewPrecision rowView:rowViewPrecision];
    if ( shoepartIndex != -1) {
        [self showTypesOfShoepartIndex:shoepartIndex];
        
        /*
         * Selector sobre el zapato
         */
        CGRect myFrame = self.selectorImage.frame;
        myFrame.origin.x = tapPoint.x-(self.selectorImage.bounds.size.width/2);
        myFrame.origin.y = tapPoint.y-(self.selectorImage.bounds.size.height/2);
        self.selectorImage.frame = myFrame;
        
        self.selectorImage.hidden = NO;

        if (mShoeAngleCurrent == ANGLE_0 && mShoepartIndexCurrent == SHOEPART_TOECAP)
            self.tagToecap.hidden = NO;
        else
            self.tagToecap.hidden = YES;
    }
    

    
}




/* ..................................................
 *
 * Despliega los detalles configurados
 *
 ....................................................*/
- (IBAction)onClickDetailsButton:(id)sender {
    
    if (!self.detailsView.hidden) {
        
        mIsShowDetails = NO;
        
        [self.showDetailsButton setTitle:@"Ver detalles" forState:UIControlStateNormal];
        self.detailsView.hidden = YES;
        
    } else {
        
        mIsShowDetails = YES;
        
        [self.showDetailsButton setTitle:@"Ocultar detalles" forState:UIControlStateNormal];
        self.detailsView.hidden = NO;
        
    }
}



/* .................
 *
 * Boton de compra
 *
 ..................*/
- (IBAction)onClickBuyButton:(id)sender {
    
    JMFSignupDummyViewController *signup = [JMFSignupDummyViewController new];
    [self.navigationController pushViewController:signup animated:YES];
}









#pragma mark - Protocolos system: Config


-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskLandscape;
    
}





#pragma mark - Metodos privados: Paleta


/* ..............................................................
 *
 * Vista para 'tipos' de una parte del zapato.
 *
 * CollectionView JMFCollectionView, titulo y llama a 'subpartes'
 *
 * -> A JMFCollectionView hay que proporcionarle:
 *          - la collectionview (vista a rellenar),
 *          - un modelo, directionScroll,
 *          - delegado (->ItemChanged),
 *          - una etiqueta (título)
 *          - y elemento actual (item check).
 *
 ...............................................................*/

-(void)showTypesOfShoepartIndex:(int)shoepartIndex {
    
    mShoepartIndexCurrent = shoepartIndex;
    [self.shoePartsSegmentControl setSelectedSegmentIndex:shoepartIndex];
    
    ModelDummy *modelTypes = [[ModelDummy alloc] initWithTypesOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex];
    
    self.shoeTypesOfShoePartCollectionView =  [[JMFCollectionView alloc]
                                               initWithCollectionView:self.collectionViewTypesOfShoePart
                                               model:modelTypes
                                               setScrollDirection:UICollectionViewScrollDirectionVertical
                                               delegate:self
                                               tag:TAG_TYPE_OF_SHOE_PART
                                               itemIndexPathCurrent:self.shoeTypeIndexPathCurrentDictionary[@(mShoepartIndexCurrent)]];
    
    
    // Titulo
    //    self.lblTypes.text = [self.shoeTypesOfShoePartCollectionView getTypeTitle];
    self.lblTypes.text = SHOE_PARTS_ARRAY_TITLE[shoepartIndex];
    
    // Subparts
    NSIndexPath *typeIndexPath = self.shoeTypeIndexPathCurrentDictionary[@(mShoepartIndexCurrent)];
    [self showShoeSubpartsWithShoepartIndexPath:mShoepartIndexCurrent shoeTypeIndex:typeIndexPath.row];
    
    // Selector
    if (self.selectorImage.hidden == NO)
        self.selectorImage.hidden = YES;
    
}


/* ..........................................................................
 *
 * Vista para 'subpartes' de un 'tipo'.
 *
 * CollectionView JMFCollectionView y llama a 'materiales'
 *
 * -> A JMFCollectionView hay que proporcionarle:
 *          - la collectionview (vista a rellenar),
 *          - un modelo, directionScroll,
 *          - delegado (->ItemChanged),
 *          - una etiqueta (título)
 *          - y elemento actual (item check).
 *
 ............................................................................*/
-(void)showShoeSubpartsWithShoepartIndexPath:(int)shoepartIndex shoeTypeIndex:(int)typeOfShoepartIndex  {
    
    
//    [self.mModel loadShoeSubpartsWithShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeOfShoepartIndex:typeOfShoepartIndex];

    ModelDummy *modelSubparts = [[ModelDummy alloc] initWithShoeSubpartsOfOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeIndex:typeOfShoepartIndex];

    self.shoeSubpartsCollectionView = [[JMFCollectionView alloc]
                                       initWithCollectionView:self.collectionViewShoeSubparts
                                       model:modelSubparts
                                       setScrollDirection:UICollectionViewScrollDirectionHorizontal
                                       delegate:self
                                       tag:TAG_SHOE_SUBPART
                                       itemIndexPathCurrent:self.shoeSubpartIndexPathCurrentDictionary[@(mShoepartIndexCurrent)]];
    
    
    // Materiales
    NSIndexPath *subpartIndex = self.shoeSubpartIndexPathCurrentDictionary[@(mShoepartIndexCurrent)];
    [self showShoeMaterialsWithShoepartIndex:shoepartIndex typeIndex:typeOfShoepartIndex subpartIndex:subpartIndex.row];
    
}





/* ..............................................................
 *
 * Vista para 'materiales' de una 'subparte'.
 *
 * CollectionView JMFCollectionView y llama a 'materiales'
 *
 * -> A JMFCollectionView hay que proporcionarle:
 *          - la collectionview (vista a rellenar),
 *          - un modelo, directionScroll,
 *          - delegado (->ItemChanged),
 *          - una etiqueta (título)
 *          - y elemento actual (item check).
 *
 ...............................................................*/

-(void)showShoeMaterialsWithShoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex  subpartIndex:(int)subpartIndex {
    
    // Collectionview
    //    [self.mModel loadMaterialsWithShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeOfShoepartIndex:typeOfShoepartIndex subpart:subpartIndex];
    
    
    self.modelMaterials = [[ModelDummy alloc] initWithMaterialsOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeIndex:typeIndex subpartIndex:subpartIndex];

    self.shoeMaterialsCollectionView = [[JMFCollectionView alloc]
                                        initWithCollectionView:self.collectionViewShoeMaterials
                                        model:self.modelMaterials
                                        setScrollDirection:UICollectionViewScrollDirectionVertical
                                        delegate:self
                                        tag:TAG_SHOE_MATERIALS
                                        itemIndexPathCurrent:[self getMaterialsIndexPathCurrent]];  // itemIndexPathCurrent:self.shoeMaterialsIndexPathCurrentDictionary[@(self.shoepartIndexCurrent)]];
}



/* ..............................................................
 *
 * Vista para 'mis materiales' de un 'tipo'.
 *
 * CollectionView JMFCollectionView.
 *
 * -> A JMFCollectionView hay que proporcionarle:
 *          - la collectionview (vista a rellenar),
 *          - un modelo, directionScroll, 
 *          - delegado (->ItemChanged),
 *          - una etiqueta (título) 
 *          - y elemento actual (item check).
 *
 ...............................................................*/
-(void)showShoeMyMaterialsWithShoepartIndexPath:(int)shoepartIndex {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:mMyMaterialsIndexCurrent inSection:0];
    self.shoeMyMaterialsCollectionView = [[JMFCollectionView alloc]
                                          initWithCollectionView:self.collectionViewMyMaterials
                                          model:self.modelMyMaterials
                                          setScrollDirection:UICollectionViewScrollDirectionHorizontal
                                          delegate:self
                                          tag:TAG_SHOE_MYMATERIALS
                                          itemIndexPathCurrent:indexPath];
    
    
    
}




/* ..........................................
 *
 * Devuelve el título de la parte del zapato.
 *
 ...........................................*/

-(NSString *) getShoepartTitle:(int) shoepartIndex {
    return SHOE_PARTS_ARRAY_TITLE[shoepartIndex];
}




/* ...............:.............
 *
 * Guarda material seleccionado.
 *
 ..............................*/
-(void)setItemMaterialsWithIndexPath:(NSIndexPath *)itemIndexPath {
    
    // Guarda estado materiales.
    [self.shoeMaterialsIndexPathCurrentDictionary setObject:itemIndexPath forKey:[self getMaterialsKeyCurrent]];
//    [mModelMyMaterials setItem:]
    
}



/* ..............................................................................
 *
 * Devuelve el 'item' actual (NSIndexpath) de la collectionview 'materiales'
 (Sección y elemeneto).
 *
 * (La elabora a partir la 'parte' del zapato del 'tipo' actuales.)
 *
 ...............................................................................*/
-(NSIndexPath *)getMaterialsIndexPathCurrent {
    
    NSString *materialsKey = [self getMaterialsKeyCurrent];
    NSIndexPath *materialsIndexPath = [self.shoeMaterialsIndexPathCurrentDictionary objectForKey:materialsKey];
    
    return materialsIndexPath;
}



/* ..............................................................................
 *
 * Devuelve la clave correspondiente al 'item' de la collectionview 'materiales'.
 *
 * (La elabora a partir la 'parte' del zapato del 'tipo' actuales.)
 *
 ...............................................................................*/
-(NSString *)getMaterialsKeyCurrent {
    
    int shoePartIndex = mShoepartIndexCurrent;
    
    NSIndexPath *shoeTypeIndexPath = self.shoeTypeIndexPathCurrentDictionary[@(mShoepartIndexCurrent)];
    int shoeTypeIndex = shoeTypeIndexPath.row;
    
    NSIndexPath *shoeSubpartIndexPath = self.shoeSubpartIndexPathCurrentDictionary[@(mShoepartIndexCurrent)];
    int shoeSubpartIndex = shoeSubpartIndexPath.row;
    
    NSString *materialsKey = [[NSString alloc] initWithFormat:@"Part:%d_Type:%d_Subpart:%d",shoePartIndex, shoeTypeIndex,  shoeSubpartIndex];
    
    return materialsKey;
}






#pragma mark - Metodos privados: Zapato

/** .......................................
 *
 *  Mueve el zapato a la siguiente seguencia
 *
 .......................................*/
-(void) moveShoe:(int)direction {
    
    self.selectorImage.hidden = YES;
    self.tagToecap.hidden = YES;
    
    int sequenceFirstNum = ANGLE_0;
    int sequenceLastNum = ANGLE_315;
    
    if (direction == DIRECTION_PANNING_DCHA)
        mShoeAngleCurrent = mShoeAngleCurrent == sequenceFirstNum ? sequenceLastNum : mShoeAngleCurrent-1;
    else
        mShoeAngleCurrent = mShoeAngleCurrent ==  sequenceLastNum ? sequenceFirstNum : mShoeAngleCurrent+1;
    
    self.shoeImage.image = [[ModelDummy new] getShoeRenderNextSequenceAngle:mShoeAngleCurrent];
    
}

/* ......................................................
 *
 * Métodos para cálcular que parte del zapato es tocada.
 *
  .......................................................*/

-(int) getShoepartTouch:(int)shoeAngle colView:(int)colView rowView:(int)rowView {
    
    int shoepart;
    if (shoeAngle ==  ANGLE_0)
        shoepart = [self getShoepartTouchAngle0:colView rowView:rowView];
    else if (shoeAngle ==  ANGLE_45)
        shoepart = [self getShoepartTouchAngle45:colView rowView:rowView];
    else if (shoeAngle ==  ANGLE_90)
        shoepart = [self getShoepartTouchAngle90:colView rowView:rowView];
    else if (shoeAngle ==  ANGLE_135)
        shoepart = [self getShoepartTouchAngle135:colView rowView:rowView];
    else if (shoeAngle ==  ANGLE_180)
        shoepart = [self getShoepartTouchAngle180:colView rowView:rowView];
    else if (shoeAngle ==  ANGLE_225)
        shoepart = [self getShoepartTouchAngle225:colView rowView:rowView];
    else if (shoeAngle ==  ANGLE_270)
        shoepart = [self getShoepartTouchAngle270:colView rowView:rowView];
    else if (shoeAngle ==  ANGLE_315)
        shoepart = [self getShoepartTouchAngle315:colView rowView:rowView];
    else
        shoepart = -1;
    
    return shoepart;
}

-(int) getShoepartTouchAngle0:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    if (colView >=3 && colView <=5 && rowView >=3 && rowView <=5 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=3 && colView <=4 && rowView >=3 && rowView <=6 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=1 && colView <=2 && rowView >=6 ) {
        shoepart = SHOEPART_TOECAP;
    }
    else if (colView >=6 && colView <=7 && rowView <=1 ) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView >=6 && rowView <=3) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView >=6 && colView <=7 && rowView >=2 && rowView <=5) {
        shoepart = SHOEPART_HEEL;
    }
    
    else
        shoepart = -1;
    
    
    return shoepart;
}


-(int) getShoepartTouchAngle45:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    if (colView >=5 && colView <=6 && rowView >=3 && rowView <=5 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=2 && colView <=4 && rowView >=3 && rowView <=6 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView <=1 && rowView >=5 ) {
        shoepart = SHOEPART_TOECAP;
    }
    else if (colView >=6 && rowView <=3 ) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView >=6 && rowView >=4 ) {
        shoepart = SHOEPART_HEEL;
    }
    
    else
        shoepart = -1;
    
    
    return shoepart;
}


-(int) getShoepartTouchAngle90:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    
    
    if (colView >=3 && colView <=4 && rowView >=1 && rowView <=4 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=2 && colView <=3 && rowView >=4 && rowView <=6 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=1 && colView <=2 && rowView >= 3 && rowView <=5 ) {
        shoepart = SHOEPART_TOECAP;
    }
    else if (colView >=5 && colView <=6 && rowView <=2 ) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView >=5 && colView <=6 && rowView >=3 ) {
        shoepart = SHOEPART_HEEL;
    }
	else
        shoepart = -1;
    
    
    return shoepart;
}



-(int) getShoepartTouchAngle135:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    
    
    if (colView >=2 && colView <=4 && rowView <=3) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView >=2 && colView <=4 && rowView >=4) {
        shoepart = SHOEPART_HEEL;
    }
    else
        shoepart = -1;
    
    
    return shoepart;
}


-(int) getShoepartTouchAngle180:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    
    
    if (colView >=3 && colView <=3 && rowView >=2 && rowView <=4 ) {
        shoepart = SHOEPART_BODY;
    }
    if (colView >=4 && colView <=5 && rowView >=3 && rowView <=6 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=6 && colView <=6 && rowView >=4) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=7 && rowView >=4) {
        shoepart = SHOEPART_TOECAP;
    }
    else if (colView <=2 && rowView <=3 ) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView <=2 && rowView >=4 ) {
        shoepart = SHOEPART_HEEL;
    }
    else
    shoepart = -1;
    
    
    return shoepart;
}

-(int) getShoepartTouchAngle225:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    
    
    if (colView >=2 && colView <=3 && rowView >=2 && rowView <=4 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=4 && colView <=5 && rowView >=5 && rowView <=6 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=6 && rowView >=5 ) {
        shoepart = SHOEPART_TOECAP;
    }
    
    else if (colView <=2 && rowView <=3 ) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView <=1 && rowView >=4 ) {
        shoepart = SHOEPART_HEEL;
    }
    else
    shoepart = -1;
    
    
    return shoepart;
}


-(int) getShoepartTouchAngle270:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    
    
    if (colView >=3 && colView <=3 && rowView >=2 && rowView <=4 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView ==3 && colView <=4 && rowView >=3 && rowView <=6 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView >=3 && colView <=4 && rowView >=3 && rowView <=6 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView ==3 && rowView ==5 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView ==2 && rowView ==2 ) {
        shoepart = SHOEPART_BODY;
    }
    else if (colView <=1 && rowView <=2) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView <=2 && rowView <=1) {
        shoepart = SHOEPART_BACK;
    }
    else if (colView >=5 && colView <=6 && rowView >=5) {
        shoepart = SHOEPART_TOECAP;
    }
    else if (colView ==2 && rowView ==4) {
        shoepart = SHOEPART_HEEL;
    }
    else
    shoepart = -1;
    
    
    return shoepart;
}

-(int) getShoepartTouchAngle315:(int)colView rowView:(int)rowView {
    
    int shoepart;
    
    
    
    if (colView >=3 && colView <=4 && rowView >=5) {
        shoepart = SHOEPART_TOECAP;
    }
    else
        shoepart = -1;
    
    
    return shoepart;
}









@end




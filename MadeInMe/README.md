   ====================
          VISTA
   ====================
 
  Es el motor para diseñar un zapato. Mediante views (segment, collectionView,...) se accede a las diferentes opciones para configurar el zapato.
 
   ______________________________________________________________
   |
   | -----------------------
   || SEGMENT CTRL (Partes) |<- Alternativa I
   | -----------------------
   | --  -------------
   ||  ||             -> CollectionView2 (Opciones de un tipo)
   ||  | ------------
   ||  | # # #
   ||<- CollectionnView1 (Tipos de una parte)
   ||  |
   ||  | # # # # # #
   ||  | # # # # # # -> CollectionView3
   ||  | # # # # # #      (Materiales)
   | --          ----------------------------------------------
   |            | Alternativa II para el segmento ctl (Partes) |
   ________________________________________________________________
 
 
 
 
   - SegmentControl: (Partes del zapato)
           @property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlPartsShoe;
 
           -> Presenta las PARTES del zapato a selecionar:Cuerpo,Puntera,Trasera,...
 
   - CollectionView1: (Tipos de una parte)
           @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTypesOfShoePart;
 
           -> Muestra los diferentes TIPOS de una PARTE del zapato (diferentes punteras por ejemplo), tras seleccionar una parte del zapato.
 
   - CollectionView2: (Opciones para un tipo)
           @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewShoeSubparts;
 
           -> OPCIONES de un TIPO.
 
   -CollectionView3: (Materiales de la opción)
           @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewMaterials;
 
           -> MATERIALES asociados a la PARTE, TIPO y OPCIÓN elegidos
 
 
 
   ============================
     Clases y Métodos llamados
   ============================
 
   Se crean tres collectionView por medio de la clase JMFCollectionView:
 
       @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTypesOfShoePart;
       @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewShoeSubparts;
       @property (weak, nonatomic) IBOutlet UICollectionView *collectionViewMaterials;
 
 
 
   Para mostrar los diferentes collectionView se utilizan los métodos:
 
                   -(void)showTypesOfShoePart:(int)key
                   -(void)showShoeSubparts:(NSString *)key
                   -(void)showMaterials:(NSString *)key
 
 
 
 
 
 
   ====================
     JMFCollectionView
   ====================
 
 
           [[JMFCollectionView alloc]
   (UICollectionView *) initWithCollectionView:self.collectionViewMaterials
                                         model:(id)model
                            setScrollDirection:(UICollectionViewScrollDirection) scrollDirection
                                       delegate:(id)aDelegate
                                   nameCallback:(NSString *)nameCallback
 
         -------------
           Protocolo:
         -------------
                     -(void)collectionViewChanged:(NSString *)nameCallback
                                    key:(NSString *)key
                                    indexPath:(NSIndexPath *)indexPath
 
               ---------------------------------------------------------------------
           -> Cada collectionView se gestiona desde la esta clase, que recibe un 
               nameCallback:(NSString *)nameCallback y un modelo (model:(id)model).
               ---------------------------------------------------------------------
 
               El nameCallback es una identificador con el que se le reconnoce en el protocolo 'collectionViewChanged'.
               EL modelo le proporciona los datos para el uso en el collectionView. Debe contener los siguientes metodos:
 
                                       * [self.model count]                      --> número de imagenes.
                                       * [self.model countSections]              --> número de secciones.
                                       * [self.model image:indexPath]            --> imagen del item.
                                       * [self.model key:indexPath]              --> 'key' asociada a cada item. Se manda al protocolo para saber identificar el item seleccionado. De esta manera se puede controlar desde el modelo como se identificará cada item idependientemente del orden en que esten presentados en la collectionView. Esta 'key' puede ser cualquier cadena que el siguiente modelo sabrá reconocer para cargar la nueva collectionView.
 
                                     model1->key1                                          model2->key2
   (model1)collectionView1(tipos) --------------> (model2:key1)collectionView2(opciones) --------------> (model3:key2)collectionView3(materials)
 
 
 
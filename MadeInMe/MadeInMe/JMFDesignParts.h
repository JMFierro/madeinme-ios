//
//  JMFDesignParts.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFDesignParts : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *secretImageView;
@property (strong, nonatomic) IBOutlet UIImageView *coverImageView;

@end

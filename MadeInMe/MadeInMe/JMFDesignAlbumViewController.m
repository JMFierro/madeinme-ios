//
//  JMFDesignViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 24/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFDesignAlbumViewController.h"
#import "JMFCollectionViewLayout.h"
#import "JMFAlbumCell.h"

static NSString *const IMG_CELL_IDENTIFIER = @"imgCell";


@interface JMFDesignAlbumViewController ()

@property (nonatomic, weak) IBOutlet JMFCollectionViewLayout *albumLayout;

@end
@implementation UINavigationController (Rotation_IOS6)


-(NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscape;
}

@end



@implementation JMFDesignAlbumViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.25f alpha:1.0f];
    
    [self.collectionView registerClass:[JMFAlbumCell class] forCellWithReuseIdentifier:IMG_CELL_IDENTIFIER];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JMFAlbumCell *imgCell = [collectionView dequeueReusableCellWithReuseIdentifier:IMG_CELL_IDENTIFIER
                                              forIndexPath:indexPath];
    
    return imgCell;
}

@end

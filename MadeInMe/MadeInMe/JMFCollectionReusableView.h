//
//  JMFCollectionReusableView.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 31/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *title;

@end

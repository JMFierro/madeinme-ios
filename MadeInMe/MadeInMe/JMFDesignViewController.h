//
//  JMFDesignViewController.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMFCollectionView.h"
#import "ExpandableNavigation.h"

@interface JMFDesignViewController : UIViewController <JMFCollectionViewDelegate> {
    UIButton* button1;
    UIButton* button2;
    UIButton* button3;
    UIButton* button4;
    UIButton* button5;
    UIButton* main;
    ExpandableNavigation* navigation;
}

@property (nonatomic, retain) IBOutlet UIButton *button1;
@property (nonatomic, retain) IBOutlet UIButton *button2;
@property (nonatomic, retain) IBOutlet UIButton *button3;
@property (nonatomic, retain) IBOutlet UIButton *button4;
@property (nonatomic, retain) IBOutlet UIButton *button5;
@property (nonatomic, retain) IBOutlet UIButton *main;

@property (retain) ExpandableNavigation* navigation;

@property (weak, nonatomic) IBOutlet UILabel *lblTypes;
- (IBAction) touchMenuItem:(id)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlPartsShoe;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTypesOfShoePart;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewShoeSubparts;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewMaterials;

- (IBAction)segmentControlShoePartChanged:(id)sender;

@end

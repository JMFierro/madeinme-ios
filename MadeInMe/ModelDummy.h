//
//  Model.h
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelDummy : NSObject

@property (strong, nonatomic) NSMutableArray *arrayOfAreasShoe;
@property (strong, nonatomic) NSMutableArray *arrayOfParts;
@property (strong, nonatomic) NSMutableArray *arrayOfMaterials;
@property int shoePartActual;

#define ANGLE_0     0
#define ANGLE_45    1
#define ANGLE_90    2
#define ANGLE_135   3
#define ANGLE_180   4
#define ANGLE_225   5
#define ANGLE_270   6
#define ANGLE_315   7



// init
-(id) initWithTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex;
-(id) initWithShoeSubpartsOfOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
-(id) initWithMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex;
-(id) initWithMyMaterialsOfShoeFamily:(int)familyIndex;

/*
 * Load datos
 */
-(NSDictionary *) loadTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex;
-(NSDictionary *) loadSubpartsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
-(NSDictionary *) loadMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex ;
-(NSDictionary *) loadMyMaterialsOfShoeFamily:(int)familyIndex;


/*
 * Otros
 */

-(NSInteger) countItems:(NSInteger) section;
-(NSInteger) countSections;

-(void) addItemFromModel:(ModelDummy *)model indexPath:(NSIndexPath *)indexPath;
-(UIImage *) getItemImage:(NSIndexPath *)indexPath;
-(UIImage *) loadItemImageView:(NSDictionary *) dataDictionary indexPath:(NSIndexPath *)indexPath;

-(UIImage *) getShoeRenderNextSequenceAngle:(int)sequence;
-(NSString *) getSectionName:(int) section;
-(NSString *) getTAG;




@end

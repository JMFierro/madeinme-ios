//
//  Model.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 25/07/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//
/* ...........................................................................................................
 *
 *  ======
 *  MODELO
 *  ======
 *
 *  Se utiliza un modelo de pruebas:
 *
 *    Para cada 'collection view' inicializa el módelo utilizando como
 *   parámetros indices que representan las posiciones actuales: familia, parte, tipo y subparte.
 *
 *      -> modelTypes = ...initWithTypesOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex];
 *      -> modelSubparts = ...initWithShoeSubpartsOfOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeIndex:typeOfShoepartIndex];
 *      -> self.modelMaterials = ...initWithMaterialsOfShoeFamily:mShoeFamilyCurrent shoepartIndex:shoepartIndex typeIndex:typeIndex subpartIndex:subpartIndex];
 *      -> self.modelMyMaterials = ...initWithMyMaterialsOfShoeFamily:mShoeFamilyCurrent];
 *
 *
 *
 *  ==============================================
 *  ** Métodos a implementar en el MODELO FINAL **
 *  ==============================================
 *
 * // init
 * -(id) initWithTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex;
 * -(id) initWithShoeSubpartsOfOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
 * -(id) initWithMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex;
 * -(id) initWithMyMaterialsOfShoeFamily:(int)familyIndex;
 *
 * // Load datos
 * -(NSDictionary *) loadTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex;
 * -(NSDictionary *) loadSubpartsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex;
 * -(NSDictionary *) loadMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex ;
 * -(NSDictionary *) loadMyMaterialsOfShoeFamily:(int)familyIndex;
 *
 *
 *  // Otros
 * -(NSInteger) countItems:(NSInteger) section;
 * -(NSInteger) countSections;
 *
 * -(void) addItemFromModel:(ModelDummy *)model indexPath:(NSIndexPath *)indexPath;
 * -(UIImage *) getItemImage:(NSIndexPath *)indexPath;
 * -(UIImage *) loadItemImageView:(NSDictionary *) dataDictionary indexPath:(NSIndexPath *)indexPath;
 *
 * -(UIImage *) getShoeRenderNextSequenceAngle:(int)sequence;
 * -(NSString *) getSectionName:(int) section;
 * -(NSString *) getTAG;
 *
 *
 ...........................................................................................................*/
#import "ModelDummy.h"
#import "Utils.h"
#import "JMFDesignViewController.h"

@interface ModelDummy()


@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) NSString *shoepartTAG;
@property (nonatomic,strong) NSDictionary *dataDictionary;


@end

@implementation ModelDummy


#pragma mark - Init 

/** .......................................................................
 *
 * Carga las imágenes de los 'tipos' disponibles para la parte del zapato.
 *
  .........................................................................*/
-(id) initWithTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex {
    
    if (self = [super init]) {
        
        _dataDictionary = [self loadTypesOfShoeFamily:familyIndex shoepartIndex:shoepartIndex];
        
    }
    return self;
}

/** ................................................................
 *
 * Carga las imágenes de las 'subpartes' disponibles para un 'tipo'.
 *
 ...................................................................*/

-(id) initWithShoeSubpartsOfOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex {
    
    if (self = [super init]) {
        
        _dataDictionary = [self loadSubpartsOfShoeFamily:familyIndex shoepartIndex:shoepartIndex typeIndex:typeIndex];
        
    }
    return self;
}


///** ................................................................
// *
// * Carga las imágenes de las 'subpartes' disponibles para un 'tipo'.
// *
// ...................................................................*/
//
//-(void) loadShoeSubpartsWithShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeOfShoepartIndex:(int)typeIndexOfShoepart {
//    
//    NSString *shoepartTAG = [[JMFDesignViewController alloc] getShoepartTitle:shoepartIndex];
//    
//    NSString *key = [[NSString alloc] initWithFormat:@"%@_subpart%d",shoepartTAG, typeIndexOfShoepart]; // ej.- @"trasera_subpart1"
//    key = [key lowercaseString];
//    _dataDictionary = [self loadData:key];
//    
//}



/** ......................................................................
 *
 * Carga las imágenes de los 'materiales' disponibles para una 'subparte'.
 *
 .........................................................................*/

-(id) initWithMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex {
    
    if (self = [super init]) {
        
        _dataDictionary = [self loadMaterialsOfShoeFamily:familyIndex shoepartIndex:shoepartIndex typeIndex:typeIndex subpartIndex:subpartIndex];
        
    }
    return self;
}



/** ......................................................................
 *
 * Carga las imágenes de los 'materiales' disponibles para una 'subparte'.
 *
 .........................................................................*/

-(id) initWithMyMaterialsOfShoeFamily:(int)familyIndex {
    
    if (self = [super init]) {
        
        _dataDictionary = [self loadMyMaterialsOfShoeFamily:familyIndex];
        
    }
    return self;
}





#pragma mark - Métodos públicos: A IMPLEMENTAR EN EL MODELO REAL (los que necesita la clase JMFColletionView)



/** .......................................................................
 *
 * Carga las imágenes de los 'tipos' disponibles para la parte del zapato.
 *
 .........................................................................*/
-(NSDictionary *) loadTypesOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex {
    
         
    NSString *shoepartTAG = [[JMFDesignViewController alloc] getShoepartTitle:shoepartIndex];
    _shoepartTAG = shoepartTAG;
    
    
    NSString *key = [[NSString alloc] initWithFormat:@"%@_type",shoepartTAG]; // ej.- @"trasera_type"
    key = [key lowercaseString];
    
    return [self loadData:key];
  
}



/** ................................................................
 *
 * Carga las imágenes de las 'subpartes' disponibles para un 'tipo'.
 *
 ...................................................................*/

-(NSDictionary *) loadSubpartsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex {
    
    
        NSString *shoepartTAG = [[JMFDesignViewController alloc] getShoepartTitle:shoepartIndex];
        
        NSString *key = [[NSString alloc] initWithFormat:@"%@_subpart%d",shoepartTAG, typeIndex]; // ej.- @"trasera_subpart1"
        key = [key lowercaseString];
    
        return [self loadData:key];
 
}



/** ......................................................................
 *
 * Carga las imágenes de los 'materiales' disponibles para una 'subparte'.
 *
 .........................................................................*/
-(NSDictionary *) loadMaterialsOfShoeFamily:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex {
    
    NSMutableDictionary *dataDictionaryTemp = [NSMutableDictionary new];
    if (shoepartIndex % 2 == 0 && subpartIndex % 2 == 0)
    {
        NSArray *dataArrayTemp = [self loadImages:@"color_ante"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"ante"];
        dataArrayTemp = [self loadImages:@"color_charol"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"charol"];
        
    }
    else if (shoepartIndex % 2 == 0 && subpartIndex % 2 != 0)
        
    {
        NSArray *dataArrayTemp = [self loadImages:@"color_charol"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"charol"];
    }
    else  if (shoepartIndex % 2 != 0 && subpartIndex % 2 == 0)
    {
        NSArray *dataArrayTemp = [self loadImages:@"color_charol"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"charol"];
    }
    else if (shoepartIndex % 2 == 0 && subpartIndex % 2 != 0)
        
    {
        
        NSArray *dataArrayTemp = [self loadImages:@"color_ante"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"ante"];
        dataArrayTemp = [self loadImages:@"color_charol"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"charol"];
    }
    else
    {
        NSArray *dataArrayTemp = [self loadImages:@"color_charol"];
        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"charol"];
    }
    
    return dataDictionaryTemp;
    
    
}



//-(void) setMymaterialsWithIndexPath:(NSIndexPath *)indexPath {
//    
//    self.dataDictionary
//}




/** .......................................... 
 *
 * Carga las imágenes de los 'mis materiales'
 *
 ..............................................*/
-(NSDictionary *) loadMyMaterialsOfShoeFamily:(int)familyIndex {
    
//    NSMutableDictionary *dataDictionaryTemp = [NSMutableDictionary new];
//  
//        NSArray *dataArrayTemp = [self loadImages:@"color_ante"];
//        [dataDictionaryTemp setObject:dataArrayTemp forKey:@"ante"];
//        dataArrayTemp = [self loadImages:@"color_charol"];
    
    NSMutableDictionary *dataDictionaryTemp = [NSMutableDictionary new];
    NSArray *dataArrayTemp = [NSArray new];    //[self loadImages:key];
    [dataDictionaryTemp setObject:dataArrayTemp forKey:@"MyMaterials"];
    
    return dataDictionaryTemp;
    
}



/** .........................................
 *
 * Añade item obteniendolo desde otro modelo
 *
 ............................................*/
-(void) addItemFromModel:(ModelDummy *)model indexPath:(NSIndexPath *)indexPath {
    
    NSString *itemString = [model item:indexPath];
    
    [self addItem:itemString];
    
}



//-(void) addItemImageView:(UIImage *) itemImageView {
//    
//    NSArray *keys = [_dataDictionary allKeys];
//    
//    // Primera sección
//    NSString *key;
//    if ([keys count]>0)
//        key = keys[0];
//    else
//        key = @"";
//    
//    NSArray *itemsArray = [_dataDictionary objectForKey:key];
//    
//    NSMutableArray *newItemsArray = [[NSMutableArray alloc] initWithArray:itemsArray];
//    [newItemsArray addObject:itemImageView];
//    
//    _dataDictionary = [NSDictionary dictionaryWithObject:newItemsArray forKey:keys[0]];
//    
//    
//}


/** ...........................
 *
 * Devuelven la imagen del item
 *
 ..............................*/

-(UIImage *) getItemImage:(NSIndexPath *)indexPath {
    
    
    return [self loadItemImageView:self.dataDictionary indexPath:indexPath];
    
}

-(UIImage *) loadItemImageView:(NSDictionary *) dataDictionary indexPath:(NSIndexPath *)indexPath {
    
   
    NSArray *dataArrayTemp = [dataDictionary objectForKey:[self section:indexPath.section]];
    NSString *itemString = dataArrayTemp [indexPath.row];
    
    return [UIImage imageNamed:itemString];
   
}




//-(UIImage *) loadItemImageView:(int)familyIndex shoepartIndex:(int)shoepartIndex typeIndex:(int)typeIndex subpartIndex:(int)subpartIndex indexPath:(NSIndexPath *)indexPath {
//    
//    NSDictionary *materialsdictionary = [self loadMaterialsOfShoeFamily:familyIndex shoepartIndex:shoepartIndex typeIndex:typeIndex subpartIndex:subpartIndex];
//    
//    return [self loadItemImageView:materialsdictionary indexPath:indexPath];
//    
//}





-(NSInteger) countItems:(NSInteger)section {
    
    NSArray *keys = [self.dataDictionary allKeys];
    
    int numItems = 0;
    if ([keys count]>0) {
        id key = [keys objectAtIndex:section];
        numItems = [[self.dataDictionary objectForKey:key] count];
    }
    
    return numItems;

}

-(NSInteger) countSections {
    
    NSInteger numSections = [[self.dataDictionary allKeys] count];
    
    if (numSections == 0)
        numSections = 1;
    
    return numSections;
}






/** ...................................................................
 *
 * Devuelve la imagen renderizada del zapato
 * correspondiente a la secuenciencia (posicion del zapato) indicada.
 *
   ....................................................................*/
-(UIImage *) getShoeRenderNextSequenceAngle:(int)sequence{
    
    
    UIImage *shoeImage;


    if (sequence == ANGLE_0)
        shoeImage = [UIImage imageNamed:@"shoerender0.png"];
    else if (sequence == ANGLE_45)
        shoeImage = [UIImage imageNamed:@"shoerender45.png"];
    else if (sequence == ANGLE_90)
        shoeImage = [UIImage imageNamed:@"shoerender90.png"];
    else if (sequence == ANGLE_135)
        shoeImage = [UIImage imageNamed:@"shoerender135.png"];
    else if (sequence == ANGLE_180)
        shoeImage = [UIImage imageNamed:@"shoerender180.png"];
    else if (sequence == ANGLE_225)
        shoeImage = [UIImage imageNamed:@"shoerender225.png"];
    else if (sequence == ANGLE_270)
        shoeImage = [UIImage imageNamed:@"shoerender270.png"];
    else if (sequence == ANGLE_315)
        shoeImage = [UIImage imageNamed:@"shoerender315.png"];
    
    return shoeImage;
}



-(NSString *) getSectionName:(int) section {
    
    // return 'Dummy'
    if (section == 0)
        return @"Ante";
    else
        return @"Charol";
}






#pragma mark - Metodos Dummy: NO ES NECESARIO IMPLEMENTARLOS EN EL MODELO REAL !

// Acceso por index
-(NSString *) section:(int) sectionIndex {
    
    // Acceso por index
    NSArray *keys = [self.dataDictionary allKeys];
    
    NSString *sectionNameString = keys[sectionIndex];
    
    return sectionNameString;
    
}

// Acceso por index
-(NSString *) item:(NSIndexPath *) indexPath {
    
    // Acceso por index
    NSArray *dataArrayTemp = [self.dataDictionary objectForKey:[self section:indexPath.section]];
    NSString *itemString = dataArrayTemp [indexPath.row];
    
    return itemString;
    
}

/* ............................................
 *
 *  Carga todas las imágenes con un prefijo.
 *
   .............................................*/
-(NSArray *)loadImages:(NSString *)filesPrefix {
    NSMutableArray *result = [NSMutableArray array];
    
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"png"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"jpg"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"jpeg"]];
    [result addObjectsFromArray:[Utils loadImagesFromAssets:filesPrefix fileType:@"gif"]];
    
    return result;
}


-(NSDictionary *) loadData:(NSString*) key {
    
    NSMutableDictionary *dataDictionaryTemp = [NSMutableDictionary new];
    NSArray *dataArrayTemp = [self loadImages:key];
    [dataDictionaryTemp setObject:dataArrayTemp forKey:key];
    
    return dataDictionaryTemp;
}

-(NSString *) getTAG {
    return self.shoepartTAG;
}

/** ..........................
 *
 * Método Dummy
 * Añade item al modelo
 * ¡Sólo a la primera sección!
 *
 .............................*/
-(void) addItem:(NSString *) itemString {
    
    NSArray *keys = [_dataDictionary allKeys];
    
    // Primera sección
    NSString *key;
    if ([keys count]>0)
        key = keys[0];
    else
        key = @"";
    
    NSArray *itemsArray = [_dataDictionary objectForKey:key];
    
    NSMutableArray *newItemsArray = [[NSMutableArray alloc] initWithArray:itemsArray];
    [newItemsArray addObject:itemString];
    
    _dataDictionary = [NSDictionary dictionaryWithObject:newItemsArray forKey:keys[0]];
    
    
}


@end

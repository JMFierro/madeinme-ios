//
//  JMFSignupDummyViewController.m
//  MadeInMe
//
//  Created by José Manuel Fierro Conchouso on 22/09/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFSignupDummyViewController.h"

@interface JMFSignupDummyViewController ()

@end

@implementation JMFSignupDummyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.signupDummyLabel.transform = CGAffineTransformMakeRotation(3.14/-10.5);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
